MythFUSE
========

``MythFUSE`` is a Python program that mounts a view of your MythTV recordings
to a specified directory.  At the moment it only supports directories local to
where ``MythFUSE`` is being run (although the database can be remote) and isn't
very smart about storage locations.

Does not run as a daemon, so run under supervisor_ or similar.

Usage
-----

``MythFUSE`` provides help with --help, but the following is all you need::

    mythfuse --username myuser --password mypassword --host dbhost --mountpoint /path/to/mount

where the username/password/host are the connection details for the MythTV
MySQL database.

Requirements
------------

``MythFUSE`` uses fusepy, sqlalchemy and fuse (of course), and has only been
tested on OpenSUSE 12.1, although it should run anywhere else these
requirements are met.

.. _supervisor: http://supervisord.org/
