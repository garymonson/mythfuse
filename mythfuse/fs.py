#!/usr/bin/env python
from mythfuse.models import Recordings, new_session
import re, argparse, os, os.path
from datetime import datetime
from collections import defaultdict
from errno import ENOENT, EACCES
from threading import Lock
from fuse import FUSE, FuseOSError, Operations

date_format = '%a %d %b %y, %I-%M-%S %p'

if not hasattr(__builtins__, 'bytes'):
    bytes = str

class MythFS(Operations):
    """
    Filesystem presenting a (read-only) navigable view of MythTV recordings.
    """

    def __init__(self, username, password, host='localhost'):
        self.username = username
        self.password = password
        self.host     = host
        self.rwlock   = Lock()

    def session(self):
        return new_session(self.username, self.password, self.host)

    def chmod(self, path, mode):
        raise FuseOSError(EACCES)

    def chown(self, path, uid, gid):
        raise FuseOSError(EACCES)

    def create(self, path, mode):
        raise FuseOSError(EACCES)

    def getattr(self, path, fh=None):
        match = re.match('^/(?:(?P<title>[^/]+)(?:/(?P<starttime>[^/]+).mpg)?)?$', path)

        session = self.session()

        try:
            if match is None:
                raise FuseOSError(ENOENT)
            elif match.group('starttime') is not None:
                # Particular recording
                try:
                    starttime = datetime.strptime(match.group('starttime'), date_format)
                except ValueError:
                    raise FuseOSError(ENOENT)

                recording = session.query(
                    Recordings
                ).filter(
                    Recordings.title == match.group('title'),
                    Recordings.starttime == starttime,
                ).first()
                stat = os.stat(os.path.join(recording.location.dirname, recording.basename))
                return dict(
                    st_mode=stat.st_mode & 0100444,
                    st_ino=stat.st_ino,
                    st_dev=stat.st_dev,
                    st_nlink=stat.st_nlink,
                    st_uid=stat.st_uid,
                    st_gid=stat.st_gid,
                    st_size=stat.st_size,
                    st_atime=stat.st_atime,
                    st_mtime=stat.st_mtime,
                    st_ctime=stat.st_ctime,
                )
            elif match.group('title') is not None:
                # Show dir
                recordings = session.query(Recordings).filter(Recordings.title == match.group('title')).order_by(Recordings.starttime)

                atime = 0
                mtime = 0
                ctime = 0
                for recording in recordings:
                    stat = os.stat(os.path.join(recording.location.dirname, recording.basename))
                    if stat.st_atime > atime:
                        atime = stat.st_atime
                    if stat.st_mtime > mtime:
                        mtime = stat.st_mtime
                    if stat.st_ctime < ctime:
                        ctime = stat.st_ctime

                try:
                    mode = stat.st_mode
                except UnboundLocalError:
                    raise FuseOSError(ENOENT)

                mode &= 040555
                # Add 'x' permission for each 'r'
                if mode & 04:
                    mode |= 01
                if mode & 040:
                    mode |= 010
                if mode & 0400:
                    mode |= 0100
                mode &= 0777
                mode |= 040000 # directory

                return dict(
                    st_mode=mode,
                    st_ino=1,
                    st_dev=31L,
                    st_nlink=2,
                    st_uid=stat.st_uid,
                    st_gid=stat.st_gid,
                    st_size=0,
                    st_atime=atime,
                    st_mtime=mtime,
                    st_ctime=ctime,
                )
            elif path == '/':
                # Root
                latest = session.query(Recordings).order_by(Recordings.starttime.desc()).first()
                stat = os.stat(latest.location.dirname)
                return dict(
                    st_mode=stat.st_mode & 040555,
                    st_ino=stat.st_ino,
                    st_dev=stat.st_dev,
                    st_nlink=stat.st_nlink,
                    st_uid=stat.st_uid,
                    st_gid=stat.st_gid,
                    st_size=stat.st_size,
                    st_atime=stat.st_atime,
                    st_mtime=stat.st_mtime,
                    st_ctime=stat.st_ctime,
                )
            else:
                raise FuseOSError(ENOENT)
        finally:
            session.close()

    def getxattr(self, path, name, position=0):
        return ''

    def listxattr(self, path):
        return []

    def mkdir(self, path, mode):
        raise FuseOSError(EACCES)

    def open(self, path, flags):
        session = self.session()

        try:
            # TODO deny access with EACCES for anything but read
            match = re.match('^/(?:(?P<title>[^/]+)(?:/(?P<starttime>[^/]+)\.mpg)?)?$', path)
            if match.group('starttime') is not None:
                starttime = datetime.strptime(match.group('starttime'), date_format)
                recording = session.query(
                    Recordings
                ).filter(
                    Recordings.title == match.group('title'),
                    Recordings.starttime == starttime,
                ).first()
                path = os.path.join(recording.location.dirname, recording.basename)

                return os.open(path, flags)

            elif match.group('title') is not None:
                raise FuseOSError(EACCES)
            else:
                raise FuseOSError(ENOENT)
        finally:
            session.close()

    def read(self, path, size, offset, fh):
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.read(fh, size)

    def readdir(self, path, fh):
        match = re.match('^/(?:(?P<title>[^/]+)/?)?$', path)
        session = self.session()

        try:
            if match.group('title') is not None:
                recordings = session.query(Recordings).filter(Recordings.title == match.group(1))
                return ['.', '..'] + [recording.starttime.strftime(date_format + '.mpg') for recording in recordings]
            elif path == '/':
                # Root
                recordings = session.query(Recordings).all()
                return ['.', '..'] + list(set([recording.title for recording in recordings]))
            else:
                raise FuseOSError(ENOENT)
        finally:
            session.close()

    def readlink(self, path):
        raise FuseOSError(EACCES)

    def removexattr(self, path, name):
        raise FuseOSError(EACCES)

    def rename(self, old, new):
        raise FuseOSError(EACCES)

    def rmdir(self, path):
        raise FuseOSError(EACCES)

    def setxattr(self, path, name, value, options, position=0):
        raise FuseOSError(EACCES)

    def statfs(self, path):
        return dict(f_bsize=512, f_blocks=4096, f_bavail=2048)

    def symlink(self, target, source):
        raise FuseOSError(EACCES)

    def truncate(self, path, length, fh=None):
        raise FuseOSError(EACCES)

    def unlink(self, path):
        raise FuseOSError(EACCES)

    def utimens(self, path, times=None):
        raise FuseOSError(EACCES)

    def write(self, path, data, offset, fh):
        raise FuseOSError(EACCES)


def main():
    parser = argparse.ArgumentParser(description='Nice filesystem view of mythtv recordings.')
    parser.add_argument('--username', required=True, help='Mythtv database username')
    parser.add_argument('--password', required=True, help='Mythtv database password')
    parser.add_argument('--host', default='localhost', help='Mythtv database host')
    parser.add_argument('--mountpoint', required=True, help='Path to mount view at')
    args = parser.parse_args()

    fuse = FUSE(MythFS(args.username, args.password, args.host), args.mountpoint, foreground=True, allow_other=True)

if __name__ == '__main__':
    main()
