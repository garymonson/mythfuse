from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker

Base = declarative_base()

def new_session(username, password, host='localhost'):
    engine = create_engine('mysql://{0}:{1}@{2}/mythconverg'.format(username, password, host), echo=False)
    Session = sessionmaker(bind=engine)
    return Session()

class StorageGroup(Base):
    __tablename__ = 'storagegroup'

    id          = Column(Integer, primary_key=True)
    groupname   = Column(String)
    hostname    = Column(String)
    dirname     = Column(String)

class Recordings(Base):
    __tablename__ = 'recorded'

    chanid      = Column(Integer, primary_key=True)
    starttime   = Column(Date, primary_key=True)
    endtime     = Column(Date, primary_key=True)
    title       = Column(String)
    description = Column(String)
    basename    = Column(String)
    filesize    = Column(Integer)

    storagegroup = Column(String, ForeignKey('storagegroup.groupname'))
    location = relationship('StorageGroup')
