from setuptools import setup, find_packages
import sys, os

version = '0.2'

setup(name='mythfuse',
      version=version,
      description="Mythtv fuse driver",
      long_description="Provides a nice filesystem view onto Mythtv recordings.",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='fuse mythtv',
      author='Gary Monson',
      author_email='gary.monson@gmail.com',
      url='https://bitbucket.org/garymonson/mythfuse',
      license='Modified BSD',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'nose',
          'fusepy',
          'sqlalchemy',
      ],
      setup_requires=[
          'nose',
      ],
      entry_points={
          'console_scripts': [
              'mythfuse = mythfuse.fs:main',
          ],
      },
      )
